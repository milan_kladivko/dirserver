package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"strings"
	"sync"
	"time"
)

var TimeStart = time.Now()

func init() { rand.Seed(TimeStart.Unix()) }

//

func ErrToString_jsonDecode(err error) string {

	var Sprt = fmt.Sprintf

	// JSON decoding errors by default (`.Error() string`) don't give much
	// info that is actionable for debugging... Let's typecheck and get
	// more context for why an error ocurred.

	var (
		syntaxError        *json.SyntaxError
		unmarshalTypeError *json.UnmarshalTypeError
	)

	switch {
	default:
		// We don't really have anything to say about the error,
		// no extra context given.
		return ""

	case errors.As(err, &syntaxError):
		// Catch any syntax errors in the JSON and send an error message
		// which interpolates the location of the problem to make it
		// easier for the client to fix.
		return Sprt(
			"Request body contains badly-formed JSON (at position %d)",
			syntaxError.Offset)

	case errors.Is(err, io.ErrUnexpectedEOF):
		// In some circumstances Decode() may also return an
		// io.ErrUnexpectedEOF error for syntax errors in the JSON. There
		// is an open issue regarding this at
		// https://github.com/golang/go/issues/25956.
		return Sprt("Request body contains badly-formed JSON")

	case errors.As(err, &unmarshalTypeError):
		// Catch any type errors, like trying to assign a string in the
		// JSON request body to a int field in our Person struct. We can
		// interpolate the relevant field name and position into the error
		// message to make it easier for the client to fix.
		return Sprt(""+
			"Request body contains an invalid value for "+
			"the %q field (at position %d)",
			unmarshalTypeError.Field,
			unmarshalTypeError.Offset)

	case strings.HasPrefix(err.Error(), "json: unknown field "):
		// Catch the error caused by extra unexpected fields in the request
		// body. We extract the field name from the error message and
		// interpolate it in our custom error message. There is an open
		// issue at https://github.com/golang/go/issues/29035 regarding
		// turning this into a sentinel error.
		fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
		return Sprt(
			"Request body contains unknown field %s",
			fieldName)

	case errors.Is(err, io.EOF):
		// An io.EOF error is returned by Decode() if the request body is
		// empty.
		return "Request body must not be empty"
	}
}

//

func findIP() string {
	var derr error
	defer func() {
		if derr != nil {
			log.Printf("%v", derr)
		}
	}()

	const FallbackTo = "localhost"

	ifaces, derr := net.Interfaces()
	if derr != nil {
		return FallbackTo
	}

	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			derr = err
			return FallbackTo
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			s := ip.String()

			if s == "127.0.0.1" || s == "::1" {
				continue // loopbacks
			} else if strings.Contains(s, ":") {
				continue // ipv4
			} else {
				return s
			}
		}
	}
	return FallbackTo
}

func sweepLANIPs(ip string) []string {

	/*
		Works by replacing the last part of the `ip` address with a number
		from 1 .. 254 and checking, for each and every one, and seeing how
		it responds.

		If it either accepts whatever port we ask for and doesn't throw an error
		or if it responds with "I don't do anything on this port, mate"
		we'll know that there's a computer running on that IP.
		Otherwise, it's unreachable or we couldn't connect in time, which happens
		only if there is nothing there and we are desperately trying anyway.

		Returns the list of IPs that have a computer we can check.
	*/

	// TODO:  Clean up a bit -- it doesn't have to be this messy.

	fourParts := strings.Split(ip, ".")
	//log.Printf("%s -> %v", ip, fourParts)

	pingOk := make([]string, 0)
	var wg sync.WaitGroup

	for i := 1; i < 254; i++ {

		a := fmt.Sprintf("%s.%s.%s.%d",
			fourParts[0], fourParts[1], fourParts[2], i)

		wg.Add(1)
		go func(addr string) {
			defer wg.Done()
			addr += ":80" // random port -- we're not testing the port itself
			const (
				Optimistic = time.Second * 2
				MoreSure   = time.Second * 6
			)
			conn, err := net.DialTimeout("tcp", addr, Optimistic)
			if err != nil {
				var responded bool
				{
					s := err.Error()
					switch {
					case strings.Contains(s, "refused"):
						responded = true // refused the port, but someone had to respond.
					case strings.Contains(s, "timeout"):
						responded = false // our timeout exceeded (pretty conclusive)
					case strings.Contains(s, "no route"):
						responded = false // our side tried everything and failed to reach
					default:
						responded = false // assume failed if other error
					}
				}
				if responded {
					//log.Printf(".....")
					// noreturn, fall through and act as if no error happened
				} else {
					//log.Printf("FAIL %s %v \n//\n%#v\n\\\\", addr, err, err) // detailed
					//log.Printf("FAIL %v", err)
					return
				}
			}

			//log.Printf(" OK  %s", addr)
			pingOk = append(pingOk, addr)

			if conn != nil { // falling through that err makes us get a nil
				conn.Close() // close right away, we just want a simple ping
			}
		}(a)
	}

	wg.Wait()

	return pingOk
}
