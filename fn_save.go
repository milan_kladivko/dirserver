package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
)

func saveAs(text []byte, r *result) {
	suffix := "saveAs: "
	log.Printf(suffix + "decoding...")

	type saveAs struct {
		TextContent string `json:"text"`
		FileName    string `json:"fname"`

		OpenEditor bool `json:"openEditor"`
	}

	var args saveAs
	err := json.Unmarshal(text, &args)
	if err != nil {
		r.err = fmt.Errorf(suffix+"bad args: %s", ErrToString_jsonDecode(err))
		r.status = http.StatusBadRequest
		return
	}
	suffix = fmt.Sprintf("saveAs[%s]: ", args.FileName)
	log.Printf(suffix + "decoded. Writing file...")

	// TODO:  Check for file with an existing name, never overwrite

	err = os.WriteFile(args.FileName, []byte(args.TextContent), os.ModePerm)
	if err != nil {
		r.err = fmt.Errorf(suffix+"filewrite error: %v", err)
		r.status = 500
		return
	}
	log.Printf(suffix + "written!")

	if args.OpenEditor {

		const Editor = "micro"
		const Term = "kitty"

		const WaitForResult = true
		done := make(chan struct{})

		var (
			suffix = fmt.Sprintf("saveAs.OpenEditor[%s]: ",
				args.FileName)
			command = fmt.Sprintf("%v",
				[]string{Term, Editor, args.FileName})
		)

		go func() {
			log.Printf(suffix + command)
			defer func() { done <- struct{}{} }()

			cmd := exec.Command(Term, Editor, args.FileName)
			err := cmd.Start()
			if err != nil {
				r.err = fmt.Errorf(suffix+"exec err: %v", err)
				return
			}
			cmd.Wait()

			log.Printf(suffix + "editor closed")
		}()

		if WaitForResult {
			<-done
			r.jsonbody(map[string]interface{}{
				"__input":       args,
				"editor":        Editor,
				"term":          Term,
				"command":       command,
				"error_or_null": fmt.Sprintf("%v", r.err),
			})
		} else {
			r.jsonbody(map[string]interface{}{
				"__input": args,
				"editor":  Editor,
				"term":    Term,
				"command": command,
			})
		}
	}
}
