package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

const HELP = `
Serve is a very simple static file server in Go.
Usage:
	-p="8100"  <port> to serve on
	-d="."     the <directory> to expose on the filesystem

Navigating to http://localhost:<port> will display the directory listing file.
`

func main() {

	log.Printf(":: bin was run with args: %v", os.Args[1:])

	port := flag.String("p", "8100", "<port> to serve on")
	directory := flag.String("d", ".", "the <directory> to expose on the filesystem")
	flag.Parse()

	//

	http.HandleFunc("/_/", // @fnstrip
		FunctionsHandler)
	http.Handle("/@/", http.StripPrefix("/@/",
		http.FileServer(http.FS(embedded_files()))))
	http.Handle("/",
		http.FileServer(http.Dir(*directory)))

	//

	ip := findIP()
	addr := "http://" + ip + ":" + *port + "/"
	dir, err := filepath.Abs(*directory)
	if err != nil {
		log.Fatalf("%v", err)
	}
	dir = dir + string(filepath.Separator)

	{ // print the LAN IPs
		oks := sweepLANIPs(ip)
		log.Printf("LAN: len %d", len(oks))
		for _, ip := range oks {
			log.Printf("- %s", ip)
		}
	}

	log.Printf("\nServer running... \n"+
		"  dir  -> %s\n"+
		"  addr -> %s",
		dir, addr)

	//

	log.Fatal(
		http.ListenAndServe(":"+*port, nil))
}
