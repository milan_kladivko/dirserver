package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

var fhandlers = map[name]fn{
	"ping": func(_ []byte, r *result) { r.jsonbody("ok") },
	"time": func(_ []byte, r *result) {
		r.jsonbody(map[string]interface{}{
			"current_time": time.Now().String(),
			"start_time":   TimeStart.String(),
			"uptime":       time.Since(TimeStart).String(),
		})
	},
	"saveAs": saveAs,
}

type fn func(jsonBytes []byte, resultToModify *result)
type result struct {
	contentType ct
	body        []byte
	status      int

	err error // probably want something better than that...
}

func FunctionsHandler(w http.ResponseWriter, r *http.Request) {
	var err error = nil
	defer func() {
		if err != nil {
			w.Write([]byte(fmt.Sprintf("SERVER ERROR : %#v", err)))
			w.WriteHeader(500)
		}
	}()

	//

	fnName := r.URL.Path[len("/_/"):] // @fnstrip
	body, err := io.ReadAll(r.Body)
	if err != nil {
		return
	}

	//

	handler, found := fhandlers[fnName]
	if !found {
		w.WriteHeader(404)
		w.Write([]byte(fmt.Sprintf(""+
			"Function not found: `%s`\n"+
			"Known Functions:\n%s",
			fnName, names(fhandlers))))
		return
	}

	defer func() {
		if panicVal := recover(); panicVal != nil {
			err = fmt.Errorf("panic: %#v", panicVal)
		}
	}()

	resu := result{
		contentType: ct_text,
		body:        []byte("Function found, did not write anything"),
		status:      500,
		err:         nil,
	}
	handler(body, &resu)
	if resu.err != nil {
		err = resu.err
		return
	}

	w.Header().Add("Content-Type", string(resu.contentType))
	w.WriteHeader(resu.status)
	w.Write(resu.body)
}

//

type name = string

func names(fns map[name]fn) string {
	var b strings.Builder
	for name := range fns {
		b.WriteString("- `" + name + "`\n")
	}
	return b.String()
}

//

func (r *result) jsonbody(o interface{}) {
	r.body, r.err = json.Marshal(o)
	if r.err != nil {
		// get more detail
		r.err = fmt.Errorf("%s", ErrToString_jsonDecode(r.err))
		r.status = 500
		return
	}
	r.status = 200
}

//

type ct string // content type

const (
	// https://www.geeksforgeeks.org/http-headers-content-type/
	ct_json ct = "application/json; charset=UTF-8"
	ct_text ct = "text/plain"
	ct_html ct = "text/html"
	ct_css  ct = "text/css"
	ct_csv  ct = "text/csv"
	ct_js   ct = "application/javascript"
)
