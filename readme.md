# TODO

- [x] The filesystem can be exposed in a basic-HTTP way. 

- [x] Expose files of the server itself `/@/`. \
      Use the `embed` std lib to include these files in the executable. 
- [x] Expose the filesystem `/`, \
      relative to where the executable is placed or where it is configured \
      to look via the program arguments.
- [x] Expose functionality using JSON communication `/_/`, \
      providing a heavily structured way to do anything parametric. 
      Let's never parse anything typed from the URL!

- [x] The used port is printed.
- [x] The used IP address on the LAN is printed (as a link, for copypasting). 

- [x] Initialize a Svelte frontend. We'll need it. 

- [ ] Expose a websocket endpoint for server data monitoring 


## Client as part of the Server executable

If we know there is another server on the LAN, the server will know best 
how to communicate with another server. We have the code, types, everything. 

We can make specialized client CLI and web app to pair with any other instance. 

### Find other instances

- [ ] Find all dirservers on the LAN.  \
      A "ping sweep" followed by a dirserver-specific test-ping should suffice. 

- [ ] Function to list all other available servers on the LAN.  \
      List all the computers too, while we're at it. 

- [ ] Periodically update (10s) the list. 

### CLI API

- [ ] Replace `httpie` and its JSON-building API. 

- [ ] Expose the types functions require. 
- [ ] Have a way to list default values of functions. 
- [ ] Have a wav to expose optional fields in types. 

- [ ] Throw errors on the client, we don't need the server to report.


## Simple strings / Simple Files

- [x] Provide an endpoint for sharing simple strings. 

- [x] Provide executable API to make the stuff super fast to work with 

  - Like... basically just a fancy `curl` that can read the clipboard
    and inject the string into the other side's clipboard. \
    Yes, KDE Connect does this, but it barely works. 

  - `httpie` provides a nice way to work with it: 
    ```
    http 10.0.0.185:8100/_/saveAs   \
        text=hello   \
        fname=./hello.txt   \
        openEditor:=true
    ```
    This is the same as sending this request: 
    ```
    POST to http://10.0.0.185:8100/_/saveAs
    Content-Type: application/json
    { "text": "hello", "fname": "./hello.txt", "openEditor": true }
    ```

  - All the APIs will be JSON-based so that we can use httpie and anything
    on the frontend. No complicated paths, no query strings, just structured
    data with expected types and type enforcement.  

- [x] Can accept sent text to the server, creating the text as a file. 
- [x] The text can be immediatelly upon saving opened in an editor. 


### Frontend page for sending strings (mobile etc)

- [ ] Create a frontend page for using this API and serve it from embedded data.
- [ ] Include a textfield for the text and an input field for the file name. 

- [ ] Autocomplete the filenames. A simple filebrowser would be nice. 

- [ ] Catch duplicate file names (don't overwrite), display errors accordingly. 
- [ ] Enable overwriting, if one so chooses. 

- [ ] Give a checkbox for editor-open. 


## Files

- [x] Has a Svelte frontend for browsing files. 
- [ ] Can create a ZIP over a directory (or multiple directories) 
      and provide a download to it.

- [ ] 
- [ ] 
- [ ] 
- [ ] 
- [ ] 