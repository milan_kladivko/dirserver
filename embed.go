package main

import (
	"embed"
	"io/fs"
	"log"
)

//go:embed embedded_files
var __embedded_files embed.FS

func embedded_files() fs.FS {

	// I don't want to embed the directory, I want to step into the dir
	// and serve that. But that requires copying the dir name here
	// and making a sub-filesystem with it. Oh well.
	// https://clavinjune.dev/en/blogs/serving-embedded-static-file-inside-subdirectory-using-go/

	dir, err := fs.Sub(__embedded_files, "embedded_files")
	if err != nil {
		log.Fatalf("%v", err)
	}
	return dir

}
